## Cenários de testes
### Utilizando a biblioteca **Netmiko**
![cenario](https://gitlab.com/senac---projeto-de-redes-de-computadores/angel-ernesto-farias-perez/-/wiki_pages/uploads/5a86d33cca10eb148cf77371a1a63c4d/teste.png)
#### Configurações iniciais dos dispositivos
[Switches e roteador](https://gitlab.com/senac---projeto-de-redes-de-computadores/angel-ernesto-farias-perez/blob/master/configINI.md)
#### Arquivos da configuração final
##### dispositivos.txt
![lista_dispositivos](img/dispositivos-arquivo.PNG)
##### cisco1.txt, arista1_config.txt, arista2_config
![config_dispositivos_netmiko](img/config_dispositivos_netmiko.PNG)

#### [Script] Configurar varios dispositivos a partir de arquivos
![codigo_1](img/codigo_1.PNG)
![codigo_2](img/codigo_2.PNG)

### Utilizando a biblioteca **Paramiko**
![cenario2](img/backup_ciscos.png)

#### Salvar backup de configuração de multiplos roteadores Cisco
![codigo_3](img/paramiko_backup.PNG)

#### Saida na IDE PyCharm
![saida_2](img/saida2.PNG)
![resultados_arquivos_backup](img/resultados_arquivos_backup.PNG)

*Arquivos de configuração extraidos do backup*
![cfg_backup](img/backup_ciscos_cfg.PNG)