#script para listar as configurações dos dispositivos
import json
import paramiko
from napalm import get_network_driver

cisco = ['192.168.56.40', '192.168.56.63']
arista = ['192.168.56.30']



for cisco_ip in cisco:
    print ('Conectando ao dispositivo Cisco ' + str(cisco_ip))
    #pega o driver do fabricante
    driver = get_network_driver('ios')
    #cria conexão e faz login
    device = driver(cisco_ip, 'admin', 'admin')
    device.open()
    facts = device.get_facts()
    #pega as informações necessárias para identificar que o fabricante
    print (json.dumps(facts,sort_keys=True, indent=4))
    #cria uma variavel unica do dispositivo que o identifique ("version: ios...")
    device.close()
    
    

    
for arista_ip in arista:
    print ('Conectando ao dispositivo Arista ' + str(arista_ip))
    driver = get_network_driver('eos')
    device = driver(arista_ip, 'admin', 'admin')
    device.open()
    facts = device.get_facts()
    print (json.dumps(facts,sort_keys=True, indent=4))
    device.close()
    
   