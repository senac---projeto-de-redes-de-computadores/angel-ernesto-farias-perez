[AUTOMAÇÃO DE REDES UTILIZANDO PYTHON](https://docplayer.com.br/74313516-Automacao-de-redes-utilizando-python.html)

[Python: Automação de tarefas](https://www.devmedia.com.br/python-automacao-de-tarefas/29984)

[Network Automation and Abstraction using Python Programming Methods](https://www.researchgate.net/publication/322017645_Network_Automation_and_Abstraction_using_Python_Programming_Methods)
*auto-abs*
[Network Automation Using Python](https://gns3.com/news/article/network-automation-using-python-)

[Why Use the GNS3 Virtual Network Simulator?](https://business.udemy.com/blog/why-use-the-gns3-virtual-network-simulator/)
*whyGNS3*
[Awesome Network Automation](https://github.com/networktocode/awesome-network-automation)

[A Use Case for Network Automation](https://www.linuxjournal.com/content/use-case-network-automation)
*linux-journal*
[NAPALM](https://napalm-automation.net/NAPALM)

[NAPALM DOCS](https://napalm.readthedocs.io/en/latest/)

[Software-Defined Networking: The New Norm for Networks](https://www.opennetworking.org/images/stories/downloads/sdn-resources/white-papers/wp-sdn-newnorm.pdf)
*SDN1*
[Programmable Networks—From Software-Defined Radio to Software-Defined Networking](https://ieeexplore.ieee.org/document/7039225)

[Netmiko Library](https://pynet.twb-tech.com/blog/automation/netmiko.html)

[NTAF IN PRACTICE](http://ntaforum.org/documents/NTAFInPractice-WP_FINAL.pdf)

[Network Programmability and Automation](https://books.google.com.br/books?id=QjJKDwAAQBAJ&printsec=frontcover&hl=pt-BR&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false)

[Design and implement Automated Procedure to upgrade remote network devices using Python](https://ieeexplore.ieee.org/document/7154701)

[ESTUDO E IMPLEMENTAÇÃO DE SOLUÇÕES PARA AUTOMAÇÃO DE DISPOSITIVOS DE REDE](https://www.ufsm.br/cursos/graduacao/santa-maria/tecnologia-em-redes-de-computadores/wp-content/uploads/sites/495/2019/05/2015-Daniel-Menzen.pdf)

[Automating Cisco Router/Switch changes with Python and Netmiko](https://www.experts-exchange.com/articles/33518/Automating-Cisco-Router-Switch-changes-with-Python-and-Netmiko.html)

[Fazendo Backup de Routers Cisco IOS utilizando Netmiko](http://netfindersbrasil.blogspot.com/2018/04/fazendo-backup-de-routers-cisco-ios.html)

[Using Python Context Managers for SSH connections](https://packetpushers.net/using-python-context-managers/)

[Netmiko and Napalm with IOS-XR: Quick Look](https://xrdocs.io/application-hosting/tutorials/2016-08-15-netmiko-and-napalm-with-ios-xr-quick-look/)

[MikroTik Automated MPLS L3VPN Lab](https://neckercube.com/index.php/2018/08/21/mikrotik-automated-mpls-l3vpn-lab/)

[What is network automation?](https://www.juniper.net/us/en/products-services/what-is/network-automation/)

[A collection of Python Code Samples for Network Management. Includes samples to run on-box and off-box](https://github.com/CiscoDevNet/python_code_samples_network)

[Use NAPALM to configure Cisco and Juniper router](https://www.bernhard-ehlers.de/blog/2017/07/04/napalm-cisco-juniper.html)

[How to use NAPALM (Python library) to Manage IOS Devices](https://web.archive.org/web/20180804090536/http://networktocode.com/labs/tutorials/how-to-use-napalm-python-library-to-manage-ios-devices)

[Network Automation using Python Programming](https://www.sevenmentor.com/blog/network-automation-using-python-programming/)

[Automate SSH connections with netmiko](https://codingnetworker.com/2016/03/automate-ssh-connections-with-netmiko/)

[Simuladores e Emuladores de Rede para o Projeto e Solução de Problemas em Ambientes de Produção](https://www.researchgate.net/publication/319402386_Simuladores_e_Emuladores_de_Rede_para_o_Projeto_e_Solucao_de_Problemas_em_Ambientes_de_Producao)

[Paramiko, Netmiko, NAPALM or Nornir?](https://blog.ipspace.net/2019/09/paramiko-netmiko-napalm-or-nornir.html)

[Do we really need network automation?](https://mirceaulinic.net/2019-01-09-do-we-need-network-automation/)

[CHOOSING A NETWORK AUTOMATION PLATFORM](https://interestingtraffic.nl/2018/12/30/choosing-a-network-automation-platform/)

[Network Automation Using Unified API – Napalm](https://blogs.cisco.com/developer/network-automation-using-napalm)