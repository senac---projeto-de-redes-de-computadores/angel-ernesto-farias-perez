![Python](rascunhos/python-logo_2x.png)

# Título

Automação de redes utilizando Python


## Discente

Angel Ernesto Farias Pérez 

## Introdução




## Motivação

O número de dispositivos em uma rede e sua natureza heterogênea está aumentando constantemente. Os métodos tradicionais usados para configuração de equipamentos de rede consomem tempo, levando em consideração também o conhecimento específico necessário para cada equipamento de diferentes fabricantes.
O principal objetivo deste artigo é demonstrar a eficiência de scripts na configuração de dispositivos de rede. Para isso sera criado uma topologia de rede emulada no GNS3, tendo como elemento principal um cliente linux, com o papel de controlador de rede. Serão controlados e configurados os dispositivos de rede usando os pacotes de código aberto Netmiko  e Paramiko , baseados em Python.

## Objetivo Geral

Apresentar um conjunto de ferramentas desenvolvidas em Python que auxiliem o administrador de rede na gerencia de redes com dispositivos de fabricantes diferentes, automatizando os processos de configuração.

## Objetivos Específicos

Objetivos
```
o
```


## Tecnologias Utilizadas

* [GNS 3](http://www.dropwizard.io/1.0.2/docs/) - Ferramenta de simulação, onde o cenário de testes será montaod
* [Netmiko](https://maven.apache.org/) - Biblioteca python para SSH
* [Paramiko](http://www.paramiko.org/) - Biblioteca python para SSH, com o foco em simplificar conexões SSH, utilizando dispositivos de multiplos fabricantes
* [Napalm](https://github.com/napalm-automation/napalm) - Biblioteca python para integrar equipamentos de multiplos fabricantes
* [Cisco IOS](https://www.cisco.com/c/pt_br/products/ios-nx-os-software/index.html) - Cisco
* [Arista EOS](https://www.arista.com/en/products/eos) - Arista


## Referencial Teórico


## Cronograma


## Referências

* 
* 

