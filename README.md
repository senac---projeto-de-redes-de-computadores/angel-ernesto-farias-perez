# Gerenciamento das configurações de switches e roteadores utilizando scripts em Python

[Wiki](https://gitlab.com/senac---projeto-de-redes-de-computadores/angel-ernesto-farias-perez/wikis/home)

## Tecnologias utilizadas

* [GNS 3](http://www.dropwizard.io/1.0.2/docs/) - Ferramenta de simulação, onde os cenários de testes foram montados
* [Netmiko](https://maven.apache.org/) - Biblioteca python para SSH
* [Paramiko](http://www.paramiko.org/) - Com o foco em simplificar conexões SSH, utilizando dispositivos de multiplos fabricantes
* [Napalm](https://github.com/napalm-automation/napalm) - Biblioteca python para integrar equipamentos de multiplos fabricantes
* [Cisco IOS](https://www.cisco.com/c/pt_br/products/ios-nx-os-software/index.html) - Foi utilizado a imagem do Cisco IOS para emular o roteador c3725
* [Arista EOS](https://www.arista.com/en/products/eos) - Foi utilizado a imagem do Arista vEOS para emular um switch Layer 3 gerenciavel
* [PyCharm Community](https://www.jetbrains.com/pycharm/) - IDE de Python de código aberto




## [Cenário de testes](cenario.md)

