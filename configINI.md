#### Switch Arista 1

```
! Command: show running-config
! device: Arista1 (vEOS, EOS-4.21.8M)
!
! boot system flash:/vEOS-lab.swi
!
transceiver qsfp default-mode 4x10G
!
hostname Arista1
!
spanning-tree mode mstp
!
enable secret sha512 $6$x5nNrTChD1vk1joz$u68mkEGo4LnQOyQj4Jz7kUCDHQuHptJjquq9K7F                                                                                                                                                                                               s14to3h54DUg1m/PSTq4MmswA7nhjuYwZbnPDw6vOjaIkJ.
no aaa root
!
username admin privilege 15 role netwrok-admin secret sha512 $6$BomwrnWaatEKgDKN                                                                                                                                                                                               $.w05R02vG7iLCGx5A5zkE/KhV8UfWA4syFSjPazuFUFlYhDlUv7FEoHuKmLxS0D18bvPTQJNPq1jSpp                                                                                                                                                                                               eOHsPl/
!
interface Ethernet1
   no switchport
   ip address 192.168.122.10/24
!
interface Ethernet2
!
interface Ethernet3
!
interface Ethernet4
!
interface Ethernet5
!
interface Ethernet6
!
interface Ethernet7
!
interface Ethernet8
!
interface Ethernet9
!
interface Ethernet10
!
interface Ethernet11
!
interface Ethernet12
!
interface Management1
   ip address 192.168.123.10/24
!
ip route 0.0.0.0/24 Ethernet1 192.168.122.37
!
no ip routing
!
end

```

#### Switch Arista 2

```
! Command: show running-config
! device: Arista2 (vEOS, EOS-4.21.8M)
!
! boot system flash:/vEOS-lab.swi
!
transceiver qsfp default-mode 4x10G
!
hostname Arista2
!
spanning-tree mode mstp
!
enable secret sha512 $6$RhljMqW7eMMvJ1X/$Y1hgnLKUKWZj3NNoL9L./W8E7OkVbtaRN22izCP09sZthqYOJVyJgUlSnp7dlkOw1suMSDsfZGF3Q/eOvHsio1
no aaa root
!
username admin privilege 15 role network-admin secret sha512 $6$SqE0aSdWz6egNwH1$qa.r6q/JmZ70PtA973fwShk16JdUDxtOZw6cPAo/HZ83XyozZBfsbk3vVRQKWOvSPnqs4mAEQ.VF.uqbfRU3R0
!
interface Ethernet1
   no switchport
   ip address 192.168.122.20/24
!
interface Ethernet2
!
interface Ethernet3
!
interface Ethernet4
!
interface Ethernet5
!
interface Ethernet6
!
interface Ethernet7
!
interface Ethernet8
!
interface Ethernet9
!
interface Ethernet10
!
interface Ethernet11
!
interface Ethernet12
!
interface Management1
   ip address 192.168.123.20/24
!
ip route 0.0.0.0/0 192.168.122.37
!
no ip routing
!
end

```

#### Cisco c3725
```
Building configuration...

Current configuration : 1821 bytes
!
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname c3725
!
boot-start-marker
boot-end-marker
!
!
aaa new-model
!
!
aaa authentication login default local
aaa authorization exec default local none
!
aaa session-id common
memory-size iomem 5
no ip icmp rate-limit unreachable
ip cef
!
!
!
!
no ip domain lookup
ip domain name python-tcc.com
ip auth-proxy max-nodata-conns 3
ip admission max-nodata-conns 3
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
username admin privilege 15 secret 5 $1$3TFe$EH5pjb/SQ/b9nRYyRWg28/
!
!
ip tcp synwait-time 5
ip scp server enable
!
!
!
!
!
interface FastEthernet0/0
 ip address 192.168.122.37 255.255.255.0
 duplex auto
 speed auto
!
interface Serial0/0
 no ip address
 shutdown
 clock rate 2000000
!
interface FastEthernet0/1
 no ip address
 shutdown
 duplex auto
 speed auto
!
interface Serial0/1
 no ip address
 shutdown
 clock rate 2000000
!
interface Serial0/2
 no ip address
 shutdown
 clock rate 2000000
!
interface Serial0/3
 no ip address
 shutdown
 clock rate 2000000
!
interface FastEthernet1/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
interface Serial2/0
 no ip address
 shutdown
 serial restart-delay 0
!
interface Serial2/1
 no ip address
 shutdown
 serial restart-delay 0
!
interface Serial2/2
 no ip address
 shutdown
 serial restart-delay 0
!
interface Serial2/3
 no ip address
 shutdown
 serial restart-delay 0
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
!
no cdp log mismatch duplex
!
!
!
control-plane
!
!
!
!
!
!
!
!
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 transport input telnet ssh
line vty 5 903
 transport input telnet ssh
!
!
end
```